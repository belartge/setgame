//
//  ViewController.swift
//  Set
//
//  Created by xcode on 09.11.2018.
//  Copyright © 2018 VSU. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var game = Game()
    var alphas:[CGFloat] = [1.0, 0.40, 0.15]
    var strokeWidths:[CGFloat] = [ -8, 8, -8]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViewFromModel()
    }
    
    @IBOutlet var cards: [UIButton]!
    

    @IBAction func cardClicked(_ sender: UIButton) {
        if let cardNumber = cards.index(of: sender) {
            
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        } else {
            print("choosen card is not in cardButton")
        }
    }
    
    @IBAction func newGameClicked(_ sender: UIButton) {
        game = Game()
        updateViewFromModel()
    }
    
    @IBAction func DealClicked(_ sender: UIButton) {
        game.deal3()
        updateViewFromModel()
    }
    
    private func updateViewFromModel() {
        updateButtonsFromModel()
    }
    
    private func updateButtonsFromModel(){        
        for index in cards.indices {
            let button = cards[index]
            if index < game.cardsOnTable.count {
                let card = game.cardsOnTable[index]
                button.setAttributedTitle(transformToNSString(card: card), for: UIControlState.normal)
            } else {
                button.setTitle("", for: UIControlState.normal)
            }
        }
    }
    
    private func transformToNSString(card: Card) -> NSAttributedString {
        let figure = multiplyString(figure: card.shape.rawValue, count: card.shapeCount.rawValue)
        let attributes:[NSAttributedStringKey : Any] = [
            .strokeColor: getColor(index: card.color.hashValue),
            .strokeWidth: strokeWidths[card.fil.hashValue],
            .foregroundColor: getColor(index: card.color.hashValue).withAlphaComponent(getFilling(index: card.fil))
        ]
        return NSAttributedString(string: figure, attributes: attributes)
    }
    
    private func multiplyString(figure: String, count: Int) -> String {
        var result = ""
        for _ in 1...count {
            result += figure
        }
        return result
    }
    
    private func getColor(index: Int) -> UIColor {
        switch index {
        case 0:
            return Colors.BLUE
        case 1:
            return Colors.GREEN
        case 2:
            return Colors.RED
        default:
            return Colors.mismatched
        }
    }
    
    private func getFilling(index: Card.Filling) -> CGFloat {
        switch index {
        case Card.Filling.EMPTY:
            return alphas[Card.Filling.EMPTY.rawValue]
        case Card.Filling.STRIPED:
            return alphas[Card.Filling.STRIPED.rawValue]
        case Card.Filling.FILED:
            return alphas[Card.Filling.FILED.rawValue]
        default:
            return alphas[Card.Filling.EMPTY.rawValue]
        }
    }

    private struct Colors {
        static let GREEN = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
        static let RED = #colorLiteral(red: 0.8459396958, green: 0.2834488153, blue: 0, alpha: 1)
        static let BLUE = #colorLiteral(red: 0.1565347006, green: 0.11825679, blue: 1, alpha: 1)
        static let hint = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        static let selected = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        static let matched = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        static let mismatched = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

    }
    
}

