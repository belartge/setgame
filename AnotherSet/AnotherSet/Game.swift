//
//  Game.swift
//  Set
//
//  Created by xcode on 09.11.2018.
//  Copyright © 2018 VSU. All rights reserved.
//

import Foundation

struct Game {
    private(set) var cardsOnTable = [Card]()
    private(set) var cardsSelected = [Card]()
    private(set) var cardsTryMatched = [Card]()
    private(set) var cardsRemoved = [Card]()
    
    private(set) var isSet: Bool? {
        set(val) {
            if val != nil{
                cardsTryMatched += cardsSelected
                cardsSelected.removeAll()
            } else {
                cardsTryMatched.removeAll()
            }
        }
        get {
            guard cardsTryMatched.count == 3 else { return nil }
            return checkForSet(card1: cardsTryMatched[0], card2: cardsTryMatched[1], card3: cardsTryMatched[2])
        }
    }
    
    var deck = Deck()
    var deckCount: Int { return deck.cards.count }
    
    init() {
        deal3()
        deal3()
        deal3()
        deal3()
        for i in cardsOnTable {
            print(i.toString())
        }
    }
    
    mutating func chooseCard(at index: Int){
//        assert(cardsOnTable.indices.contains(index),
//               "SetGame.chooseCard(at: \(index)) : Choosen index out of range")
        let cardChoosen = cardsOnTable[index]
        if !cardsRemoved.contains(cardChoosen) && !cardsTryMatched.contains(cardChoosen) {
            if isSet != nil {
                if isSet! { replaceOrRemove3Cards() }
                isSet = nil
            }
            if cardsSelected.count == 2, !cardsSelected.contains(cardChoosen) {
                cardsSelected += [cardChoosen]
                isSet = checkForSet(card1: cardsSelected[0], card2: cardsSelected[1], card3: cardsSelected[2])
            } else {
                cardsSelected.inOut(element: cardChoosen)
            }
        }
    }
    
    private mutating func take3FromDeck() -> [Card]? {
        var threeCards = [Card]()
        for _ in 0...2 {
            if let card = deck.draw() {
                threeCards.append(card)
            }
        }
        if threeCards.count > 0 {
            return threeCards
        } else {
            return nil
        }
    }
    
    mutating func deal3() {
        if let deal3Cards = take3FromDeck() {
            cardsOnTable += deal3Cards
        }
    }
    
    private mutating func replaceOrRemove3Cards(){
        if let take3Cards = take3FromDeck() {
            cardsOnTable.replace(elements: cardsTryMatched, new: take3Cards)
        } else {
            cardsOnTable.remove(elements: cardsTryMatched)
        }
        cardsRemoved += cardsTryMatched
        cardsTryMatched.removeAll()
    }
    
    func areAllEqual(param1: Int, param2: Int, param3: Int) -> Bool {
        return param1 == param2 && param2 == param3
    }
    
    func areAllDifferent(param1: Int, param2: Int, param3: Int) -> Bool {
        return param1 != param2 && param2 != param3 && param1 != param3
    }
    
    func checkForSet(card1: Card, card2: Card, card3: Card) -> Bool {
        if !areAllEqual(param1: card1.color.hashValue, param2: card2.color.hashValue, param3: card3.color.hashValue) && !areAllDifferent(param1: card1.color.hashValue, param2: card2.color.hashValue, param3: card3.color.hashValue) {
            return false
        }
        if !areAllEqual(param1: card1.fil.hashValue, param2: card2.fil.hashValue, param3: card3.fil.hashValue) && !areAllDifferent(param1: card1.fil.hashValue, param2: card2.fil.hashValue, param3: card3.fil.hashValue) {
            return false
        }
        if !areAllEqual(param1: card1.shape.hashValue, param2: card2.shape.hashValue, param3: card3.shape.hashValue) && !areAllDifferent(param1: card1.shape.hashValue, param2: card2.shape.hashValue, param3: card3.shape.hashValue) {
            return false
        }
        if !areAllEqual(param1: card1.shapeCount.hashValue, param2: card2.shapeCount.hashValue, param3: card3.shapeCount.hashValue) && !areAllDifferent(param1: card1.shapeCount.hashValue, param2: card2.shapeCount.hashValue, param3: card3.shapeCount.hashValue) {
            return false
        }
        
        return true
    }
}

extension Array where Element : Equatable {
    mutating func inOut(element: Element) {
        if self.count > 0 {
            for i in 0...self.count-1 {
                if self[i] == element {
                    self.remove(at: i)
                    return
                }
            }
        }
        self.append(element)
       
    }
    
    mutating func replace(elements: [Element], new: [Element]){
        guard elements.count == new.count else { return }
        for idx in 0..<new.count {
            if self.count > 0 {
                for i in 0...self.count-1 {
                    if self[i] == elements[idx] {
                        self[i] = new[idx]
                    }
                }
            }
        }
    }
    
    mutating func remove(elements: [Element]){
        self = self.filter({ !elements.contains($0) })
    }
}


