////
////  CardButton.swift
////  AnotherSet
////
////  Created by xcode on 07.12.2018.
////  Copyright © 2018 VSU. All rights reserved.
////
//
import UIKit
//
//class CardButton: UIButton {
//
//    /*
//    // Only override draw() if you perform custom drawing.
//    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//        // Drawing code
//    }
//    */
//
//    @IBInspectable var borderColor: UIColor = DefaultValues.borderColor {
//        didSet {
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//
//    @IBInspectable var borderWidth: CGFloat = DefaultValues.borderWidth {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//
//    @IBInspectable var cornerRadius:CGFloat = DefaultValues.cornerRadius {
//        didSet {
//            layer.cornerRadius = cornerRadius
//        }
//    }
//
//    override init(frame: CGRect){
//        super.init(frame: frame)
//        configure()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        configure()
//    }
//
//    func configure(){
//        layer.borderColor = borderColor.cgColor
//        layer.borderWidth = borderWidth
//        layer.cornerRadius = cornerRadius
//        clipsToBounds = true
//    }
//
//    struct DefaultValues {
//        static let borderColor: UIColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
//        static let borderWidth: CGFloat = 4.0
//        static let cornerRadius: CGFloat = 8.0
//    }
//
//    var colors = [#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)]
//    var alphas: [CGFloat] = [1.0, 0.4, 0.15]
//    var strokeWidths: [CGFloat] = [-8, 8, -8]
//    var symbols = ["▲", "●", "■"]
//
//    var cardModel: Card? { didSet { updateButton() } }
//
//    private func updateButton() {
//        if let card = cardModel {
//            let attributedString = setAttributedString(card: card)
//            setAttributedTitle(attributedString, for: .normal)
//            backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//            isEnabled = true
//        } else {
//            setAttributedTitle(nil, for: .normal)
//            setTitle(nil, for: .normal)
//            backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
//            borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
//            isEnabled = false
//        }
//    }
//
//    private var verticalSizeClass: UIUserInterfaceSizeClass {
//        return UIScreen.main.traitCollection.verticalSizeClass
//    }
//
//    private func setAttributedString(card: Card) -> NSAttributedString {
//        let symbol = symbols[card.shape.rawValue]
//        let separator = verticalSizeClass == .regular ? "\n" : " "
//        let symbolsString = symbol.join(n: card.shapeCount.rawValue, with: separator)
//        let attributes: [NSAttributedString.Key: Any] = [
//            .strokeColor: colors[card.color.rawValue],
//            .strokeWidth: strokeWidths[card.fil.rawValue],
//            .foregroundColor: colors[card.color.idx].withAlphaComponent(alphas[card.fil.rawValue])
//        ]
//        return NSAttributedString(string: symbolsString, attributes: attributes)
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        updateButton()
//    }
//
//    func setBorderColor(color: UIColor) {
//        borderColor = color
//        borderWidth = Constants.borderWidth
//    }
//
//    private struct Constants {
//        static let cornerRadius: CGFloat = 8.0
//        static let borderWidth: CGFloat = 5.0
//        static let borderColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
//    }
//}
//
//extension String {
//    func join(n: Int, with separator: String) -> String {
//        guard n > 1 else {return self}
//        var symbols = [String]()
//        for _ in 0..<n {
//            symbols += [self]
//        }
//        return symbols.joined(separator: separator)
//    }
//}

