//
//  Card.swift
//  Set
//
//  Created by xcode on 09.11.2018.
//  Copyright © 2018 VSU. All rights reserved.
//

import Foundation
import UIKit

struct Card: Equatable {
    
    private var identifier: Int
    static var identifierFactory = 0
    
    var color: Colors
    var shapeCount: Counts
    var shape: Shapes
    var fil: Filling
    
    private var isClicked = false
    var isMatched = false
    
    init(color: Colors, shape: Shapes, count: Counts, filledDegree: Filling) {
        self.identifier = Card.getUniqueIdentifier()
        self.color = color
        self.shape = shape
        self.shapeCount = count
        self.fil = filledDegree
    }
    
    func checkIfClicked() -> Bool {
        return isClicked
    }
    
    mutating func reverse() -> Bool {
        self.isClicked = !self.isClicked
        return isClicked
    }
    
    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    func toString() -> String {
        return "\(self.color)\(self.shapeCount)\(self.shape)\(self.fil)"
    }
    
    enum Colors: Int {
        case GREEN
        case RED
        case VIOLET
        
        static var all: [Colors] {
            let allColors: [Colors] = [Colors.GREEN, .RED, .VIOLET]
            return allColors
        }
    }
    
    enum Shapes: String {
        case DIAMOND = "■"
        case OVAL = "●"
        case WAVE = "▲"
        
        static var all: [Shapes] {
            let allShapes: [Shapes] = [.DIAMOND, .OVAL, .WAVE]
            return allShapes
        }
    }
    
    enum Filling: Int {
        case STRIPED
        case FILED
        case EMPTY
        
        static var all: [Filling] {
            let allFillings: [Filling] = [Filling.STRIPED, .FILED, .EMPTY]
            return allFillings
        }
    }
    
    enum Counts: Int {
        case ONE = 1
        case TWO = 2
        case THREE = 3
        
        static var all: [Counts] {
            let allCounts: [Counts] = [.ONE, .TWO, .THREE]
            return allCounts
        }
    }
    
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return (
            (lhs.shape == rhs.shape) &&
                (lhs.color == rhs.color) &&
                (lhs.shapeCount == rhs.shapeCount) &&
                (lhs.fil == rhs.fil)
        )
    }
}

