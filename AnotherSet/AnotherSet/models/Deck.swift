//
//  Deck.swift
//  AnotherSet
//
//  Created by xcode on 30.11.2018.
//  Copyright © 2018 VSU. All rights reserved.
//

import Foundation

struct Deck {
    private(set) var cards = [Card]()
    
    init() {
        generateCards()
    }
    
    mutating func generateCards() {
        for color in Card.Colors.all {
            for form in Card.Shapes.all {
                for count in Card.Counts.all {
                    for fill in Card.Filling.all {
                        cards.append(Card(color:color, shape: form, count: count, filledDegree:fill))
                    }
                }
            }
        }
    }
    
    mutating func draw() -> Card? {
        if(cards.count > 0){
            return cards.remove(at: cards.count.arc4random)
        } else {
            return nil
        }
    }
}
